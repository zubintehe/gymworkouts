# == Route Map
#
#                   Prefix Verb   URI Pattern                    Controller#Action
#         new_user_session GET    /users/sign_in(.:format)       users/sessions#new
#             user_session POST   /users/sign_in(.:format)       users/sessions#create
#     destroy_user_session DELETE /users/sign_out(.:format)      users/sessions#destroy
#            user_password POST   /users/password(.:format)      users/passwords#create
#        new_user_password GET    /users/password/new(.:format)  users/passwords#new
#       edit_user_password GET    /users/password/edit(.:format) users/passwords#edit
#                          PATCH  /users/password(.:format)      users/passwords#update
#                          PUT    /users/password(.:format)      users/passwords#update
# cancel_user_registration GET    /users/cancel(.:format)        users/registrations#cancel
#        user_registration POST   /users(.:format)               users/registrations#create
#    new_user_registration GET    /users/sign_up(.:format)       users/registrations#new
#   edit_user_registration GET    /users/edit(.:format)          users/registrations#edit
#                          PATCH  /users(.:format)               users/registrations#update
#                          PUT    /users(.:format)               users/registrations#update
#                          DELETE /users(.:format)               users/registrations#destroy
#                     root GET    /                              users/registrations#new
#                 workouts GET    /workouts(.:format)            workouts#index
#                          POST   /workouts(.:format)            workouts#create
#              new_workout GET    /workouts/new(.:format)        workouts#new
#             edit_workout GET    /workouts/:id/edit(.:format)   workouts#edit
#                  workout GET    /workouts/:id(.:format)        workouts#show
#                          PATCH  /workouts/:id(.:format)        workouts#update
#                          PUT    /workouts/:id(.:format)        workouts#update
#                          DELETE /workouts/:id(.:format)        workouts#destroy
#

Rails.application.routes.draw do
  devise_for :users, controllers: {
        sessions: 'users/sessions',
        confirmations: 'users/confirmations',
        omniauthable: 'users/omniauth_callbacks',
        passwords: 'users/passwords',
        registrations: 'users/registrations',
        unlock: 'users/unlocks'
  }

  devise_scope :user do
    root "users/registrations#new"
    resources :workouts
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

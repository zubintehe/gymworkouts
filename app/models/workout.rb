# == Schema Information
#
# Table name: workouts
#
#  id      :integer          not null, primary key
#  user_id :integer
#  type    :string
#  name    :string
#
# Indexes
#
#  index_workouts_on_type     (type) UNIQUE
#  index_workouts_on_user_id  (user_id)
#

class Workout < ActiveRecord::Base
  belongs_to :user
  has_many :exercises
  enum type: [:upper, :lower, :cardio]
end

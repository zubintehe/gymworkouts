# == Schema Information
#
# Table name: exercises
#
#  id         :integer          not null, primary key
#  workout_id :integer
#  name       :string
#  reps       :integer
#  sets       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_exercises_on_workout_id  (workout_id)
#

class Exercise < ActiveRecord::Base
end

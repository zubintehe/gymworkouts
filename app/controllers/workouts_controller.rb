class WorkoutsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @workouts = Workout.all
  end

  def new
  end

  def create
    @workout = current_user.workouts.build(workout_params)
    if @workout.save
      flash[:success]  = "Workout created"
      redirect_to workouts_url
    else
      render 'workouts/new'
    end
  end


  private

    def workout_param
      params.require(:workout).permit(:type)
    end
end

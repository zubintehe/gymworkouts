# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
zubin = User.create({
    email: 'test@gmail.com',
    first_name: "First_name",
    last_name: "Last_name",
    password: "testing123",
    password_confirmation: "testing123",
    phone: "4695444655"
});

zubin.workouts = {
  type: 'upper'
}

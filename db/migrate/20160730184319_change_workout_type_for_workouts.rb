class ChangeWorkoutTypeForWorkouts < ActiveRecord::Migration
  def change
    rename_column :workouts, :workout_type, :type
  end
end

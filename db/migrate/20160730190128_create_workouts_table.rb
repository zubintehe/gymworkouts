class CreateWorkoutsTable < ActiveRecord::Migration
  def change
    create_table :workouts do |t|
      t.belongs_to :user, index: true
      t.string :type
    end

    add_index :workouts, :type, unique: true
  end
end

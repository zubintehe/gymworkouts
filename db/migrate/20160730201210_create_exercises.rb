class CreateExercises < ActiveRecord::Migration
  def change
    create_table :exercises do |t|
      t.belongs_to :workout, index: true
      t.string :name
      t.integer :reps
      t.integer :sets
      t.timestamps null: false
    end
  end
end

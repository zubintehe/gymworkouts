class Workouts < ActiveRecord::Migration
  def change
    create_table :workouts do |t|
      t.string :workout_type
    end

    add_index :workouts, :workout_type, unique: true
  end
end
